"use strict"

document.onclick = function (event) {
    if (event.target.classList.contains("services_tabs-title")) {
        document.querySelector(".services_tabs-title.active-s").classList.remove("active-s");
        event.target.classList.add("active-s");

        document.querySelector(".tabs-text.active-s").classList.remove("active-s");
        let ourId = event.target.getAttribute("data-serv");
        document.getElementById(ourId).classList.add("active-s");

    } else if (event.target.classList.contains("amazing_work_tabs-title")) {
        document.querySelector(".amazing_work_tabs-title.active").classList.remove("active");
        event.target.classList.add("active");

        $(".tabs_images_item_link.active").removeClass('active');
        let newId = event.target.getAttribute("data-tab");
        let selector = ".tabs_images_item_link[data-rel='" + newId + "']";
        if (newId == 'all') {
            selector = ".tabs_images_item_link";
        }
        $(selector).slice(0, 8).addClass('active');

    }
}


function scrollFunction() {
    let header = document.querySelector(".page_header")
    if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 20) {
        header.classList.add('page_header--innerpage');
    } else {
        header.classList.remove('page_header--innerpage')
    }
}

if (!location.search) {
    window.onscroll = function () {
        scrollFunction();
    };
    scrollFunction();
}
const url = decodeURIComponent(document.location.pathname);
const hash = location.hash.replace(/^#/, '');

if (hash) {
    $("a[href='" + url + "#" + hash + "']").addClass("menu_link--active");
} else {
    $("a[href='" + url + "']").addClass("menu_link--active");
}

$('.menu_link').click(function () {
    $(".menu_link--active").removeClass("menu_link--active");
    $(this).addClass("menu_link--active");
});

jQuery('form').submit(function (event) {
    event.preventDefault();
    const url = jQuery(this).attr('action');

    const formData = new FormData(jQuery(this).get(0));

    jQuery.ajax({
        url: url,
        type: "POST",
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
        success: function (data) {
            $('form').slideUp();
            $('.form-success').slideDown();
        }
    });
    return false;
});