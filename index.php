<?php

if (!empty($_POST['name'])) {
    //Если отправлена форма

    //Соединяемся с Базой Данных
    $connect = mysqli_connect('localhost', 'root', 'Postcop95.');
    mysqli_select_db($connect, 'realestate');
    mysqli_query($connect, 'SET NAMES utf8mb4');

    //Выполняем SQL записи в базу
    $sql = "
    INSERT INTO `orders` 
    SET 
    `name` = '{$_POST['name']}',
    `email` = '{$_POST['email']}',
    `phone` = '{$_POST['phone']}',
    `added` = NOW();
    ";
    mysqli_query($connect, $sql);
    exit;
}

$pageName = $_GET['page'] ?? 'home';

$fileName = 'templates/site/' . $pageName . '.html';

if (file_exists($fileName)) {
    $mainContent = file_get_contents($fileName);
} else {
    $mainContent = file_get_contents('templates/site/404.html');
}

$template = file_get_contents('templates/site/index.html');

$template = str_replace('{{center}}', $mainContent, $template);
if ($pageName === 'home') {
    $template = str_replace('page_header--innerpage', '', $template);
}

echo $template;
